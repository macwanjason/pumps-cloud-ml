# This scripts declares all variables needed to run the scripts in the scripts folder

# change this to match your own settings
export BUCKET_NAME=pumps-cloud-ml
export GOOGLE_APPLICATION_CREDENTIALS=/home/jason/Projects/pumps-cloud-ml/pumps-cloud-ml-056881873ae9.json

#--------------------------------------------------------------------------------------------------

# Google Cloud MLE job settings for training and deploying models
export JOB_NAME=job_$(date +"%Y%m%d")
export JOB_DIR=gs://${BUCKET_NAME}/jobs/${JOB_NAME}
export PYTHON_VERSION="2.7"
export REGION="us-east1"
export RUNTIME_VERSION="1.9"
# change version number as you add new versions
export MODEL_VERSION="pumps_1_0"
export MODEL_NAME="pumps"
# your job name will be different so change this
export MODEL_LOCATION=gs://${BUCKET_NAME}/jobs/job_20181012_113028/models/
export MODEL_DESC="Random Forest model to predict water pump repairs in Tanzania"

# python application settings for training locally
export LOCAL_DATA_FILE=gs://${BUCKET_NAME}/data/pumps_data.csv
export LOCAL_MODEL_DIR=jobs/local/models
export LOCAL_TRANSFORMER_DIR=jobs/local/transformers

# python application settings for training on GC MLE
export DATA_FILE=gs://${BUCKET_NAME}/data/pumps_data.csv
export MODEL_DIR=jobs/${JOB_NAME}/models
export TRANSFORMER_DIR=jobs/${JOB_NAME}/transformers

