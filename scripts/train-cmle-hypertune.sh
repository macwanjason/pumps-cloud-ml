# This script deploys the pumps application for training the model
# on Google Cloud ML Engine with hyperparameter tuning. Takes about
# 10 mins to run

# import settings
. ${PWD}/config.sh

# point to project root directory
parentdir="$(dirname "${PWD}")"

# deploy training
gcloud ml-engine jobs submit training ${JOB_NAME} \
    --module-name trainer.task \
    --config config.yaml \
    --job-dir ${JOB_DIR} \
    --package-path $parentdir/pumps/trainer \
    --python-version ${PYTHON_VERSION} \
    --region ${REGION} \
    --runtime-version ${RUNTIME_VERSION} \
    -- \
    --bucket=${BUCKET_NAME}  \
    --model_dir=${MODEL_DIR} \
    --transformer_dir=${TRANSFORMER_DIR} \
    --data=${DATA_FILE}
