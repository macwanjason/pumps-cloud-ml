# This script deploys the pumps application for training the model
# on Google Cloud ML Engine without hyperparameter tuning. Takes
# about 5 mins to run

# import settings
. ${PWD}/config.sh

# point to project root directory
parentdir="$(dirname "${PWD}")"

# deploy training
gcloud ml-engine jobs submit training ${JOB_NAME} \
    --module-name trainer.task \
    --job-dir ${JOB_DIR} \
    --package-path $parentdir/pumps/trainer \
    --python-version ${PYTHON_VERSION} \
    --region ${REGION} \
    --runtime-version ${RUNTIME_VERSION} \
    -- \
    --bucket=${BUCKET_NAME}  \
    --model_dir=${MODEL_DIR} \
    --transformer_dir=${TRANSFORMER_DIR} \
    --data=${DATA_FILE} \
    --n_estimators=100 \
    --class_weight='balanced_subsample' \
    --criterion='entropy'
