# Deploy a trained model in the cloud with Cloud ML Engine to serve predictions.

# import settings
. ${PWD}/config.sh

# create the model resource
echo "Creating model resource..."
gcloud ml-engine models create ${MODEL_NAME} \
    --description "${MODEL_DESC}" \
    --enable-logging \
    --regions ${REGION}

#gcloud ml-engine models delete ${MODEL_NAME}
#gcloud ml-engine models create ${MODEL_NAME} --regions $REGION