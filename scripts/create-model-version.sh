# Create a model version once a model resource has been instantized

# import settings
. ${PWD}/config.sh

# create the first version
echo "Deleting and deploying $MODEL_NAME $MODEL_VERSION from $MODEL_LOCATION ... this will take a few minutes"
# create the model resource
gcloud ml-engine versions create ${MODEL_VERSION} \
    --model ${MODEL_NAME} \
    --framework SCIKIT_LEARN  \
    --origin ${MODEL_LOCATION} \
    --python-version=${PYTHON_VERSION} \
    --runtime-version ${RUNTIME_VERSION}

#gcloud ml-engine versions delete ${MODEL_VERSION} --model ${MODEL_NAME}