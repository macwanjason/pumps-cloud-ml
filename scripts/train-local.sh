# This script trains the ML algorithm locally on your machine


# import settings
. ${PWD}/config.sh

# set the pumps directory to PYTHONPATH
parentdir="$(dirname "${PWD}")"
export PYTHONPATH=${PYTHONPATH}:$parentdir/pumps

# run the application
python -m trainer.task \
    --bucket=${BUCKET_NAME}  \
    --job-dir=gs://${BUCKET_NAME}/jobs/local \
    --model_dir=${LOCAL_MODEL_DIR} \
    --transformer_dir=${LOCAL_TRANSFORMER_DIR} \
    --data=${LOCAL_DATA_FILE} \
    --n_estimators=100 \
    --class_weight='balanced_subsample' \
    --criterion='entropy'