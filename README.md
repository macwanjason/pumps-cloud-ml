# DEVELOPING MACHINE LEARNING MODELS WITH GOOGLE CLOUD DATALAB AND GOOGLE CLOUD ML ENGINE

This repository contains a working example of how to train and deploy a machine learning model on Google Cloud ML Engine (CMLE) using either a GCP Cloud Datalab or a local machine environment.

## PROJECT STRUCTURE
```
pumps-cloud-ml
├── data                              -> folder containing sample data
│   └── pumps_data.csv              
├── pumps                             -> project root for CMLE application
│   ├── setup.py                      -> setup file for CMLE job
│   └── trainer                       -> main module for CMLE job
│       ├── __init__.py               -> makes the directory a Python module
│       ├── model.py                  -> main machine learning script
│       └── task.py                   -> entrypoint for the CMLE application
├── scripts                           -> bash scripts to run CMLE jobs or train locally
│       ├── create-model-resource.sh   
│       ├── create-model-version.sh                      
│       ├── train-local.sh 
│       ├── train-cmle.sh 
│       ├── train-cmle-hypertune.sh     
│       └── config.yaml               -> configuration for CMLE when training 
├── predictions                       -> Python scripts to run predictions
│       ├── config.py   
│       ├── predict_batch.py          -> currently does not work (CMLE does not support 
│       │                                sklearn batch prediction as of this writing)
│       └───predict_online.py   
├── pumps-notebook.ipynb              -> GC Datalab notebook
├── environment.yaml                  -> create a local conda env with this yaml
└── README.md               
```

## SETUP FOR GCP DATALAB
1. Follow this [Quickstart](https://cloud.google.com/datalab/docs/quickstart) to create a Google Datalab instance and to install Google SDK on your machine
2. Go to your GCP console in your browser and enable the Google Cloud Machine Learning Engine API from the navigation menu
3. Following instructions from step.1, start your Google Datalab service.
4. Open the application in your browser. On the top right menu, click on the ungit button. This will open a new tab with the ungit interface.
5. In the address bar type `/content/` and press enter. You should see a `'content' is not a repository` message along with some buttons/options to make a repository.
6. In the `Clone from` field enter the url for this repo `https://github.com/macwanjason/pumps-cloud-ml.git` and accept the default auto populated value `pumps-cloud-ml` in the `to` field. Click on Clone repository
7. Once the repo is cloned, you will see ungit's branch graphic. In the address bar, you will see a `+` icon to the right. Click on it to add this repo to the ungit homepage.
8. At this point, if you go back to the Datalab page and navigate to the root directory, you will see the `pumps-cloud-ml` folder with all the code in it.
9. Open the `pumps-notebook.ipynb` file and work through the code.

## SETUP FOR LOCAL MACHINE
Note, this code repo has been tested on a laptop running Ubuntu and assumes you have Conda or Miniconda installed in your user folder.

1. If you haven't already done so, install Google SDK on your local machine and also enable the Google Cloud Machine Learning Engine API from GCP Console. Also create a bucket with the same name as your project name.
2. Copy dataset to your bucket. Run this from the project root folder. Replace `pumps-cloud-ml` with your bucket name:  
    `gsutil cp ./data/pumps_data.csv gs://pumps-cloud-ml/data/pumps_data.csv`
2. `cd` into the project root folder and create a conda environment with `conda env create -n pumps-cloud-ml -f environment.yaml` 
3. Set up Google Cloud SDK by running `gcloud init`
4. When you enable the GC MLE API, a compute instance service account is created for the GCP project. In Google Console go to: IAM & admin>Service accounts. You will see a compute service account.
5. Click on the three dots to the right of the service account and select `Create key`. Choose json and download it to the project root folder `pumps-cloud-ml`.
6. Change the `BUCKET_NAME` and `GOOGLE_APPLICATION_CREDENTIALS` in the `pumps-cloud-ml/scripts/project-settings.sh` file. Point the `GOOGLE_APPLICATION_CREDENTIALS` to the json key you downloaded in step 5. Make changes to any other variable as needed, e.g. deploying a model requires pointing to `MODEL_LOCATION` where the pickled model lives.
7. activate the conda environment: `source activate pumps-cloud-ml`
8. `cd` into the scripts folder and run `sh train-local.sh` or whichever other script you want to run.


### CONFIGURE PYCHARM (OPTIONAL)

If you use Pycharm, you can download the Bash Support plugin to create bash run configurations.

1. Open terminal and `source activate pumps-cloud-ml`
2. Get environment variables by `printenv`
3. Copy the value of `PATH` and create an environment variable in a bash run configuration in Pycharm. For example:  

    Name: `Train Local`  
    Script: `/home/jason/Projects/pumps-cloud-ml/scripts/train-local.sh`  
    Working Directory: `/home/jason/Projects/pumps-cloud-ml/scripts`  

    Environment variables:  
    Name: `PATH`  
    Value: `/home/jason/miniconda3/envs/pumps-cloud-ml/bin:/home/jason/miniconda3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin `  
    Include Parent environment variables: click for yes

Note, that you will not be able to run the notebook as it was developed for use only on GCP Datalab.I recommend setting up the project in Pycharm.