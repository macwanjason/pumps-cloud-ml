# -*- coding: utf-8 -*-
"""
Created on: 12-10-2018 
@author: jason

NOTE: CMLE does not yet suppport batch predictions (as of Oct 2018)
so this script will not work and is simply meant for future reference.

"""
import os
import pandas as pd
from config import Config
from sklearn.externals import joblib
from tensorflow.python.lib.io import file_io
from google.cloud import storage
from googleapiclient import discovery, errors


# to authenticate with Google Cloud, you need to set Google Creds as an environment variable
# replace the json reference with your file
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = Config.GOOGLE_APPLICATION_CREDENTIALS

# -------------------------------------------------------------------------------------------------


def load_model(bucket, gcspath, file_name):

    """
    :param bucket    : the bucket name where the model is stored
    :param gcspath   : path in the bucket where the model is stored
                       without gs:// or the bucket name
    :param file_name : the filename of the model you want to download
    :return          : an unpickled object stored in memory
    """

    # instantiate a GCS client and point to bucket
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket)

    # download from bucket
    model_path = os.path.join(gcspath, file_name)
    blob = bucket.blob(model_path)
    blob.download_to_filename(file_name)

    # load into memory
    obj = joblib.load(file_name)
    os.remove(file_name)

    return obj


def pre_processing(x):
    """
    A full pipeline that cleans and preprocesses data for batch
    prediction in CMLE

    :param x: full GCS path to file
              e.g: 'gs://bucket_name/data/data.csv'
    :return:
    """

    useful_columns = ['amount_tsh',
                      'gps_height',
                      'longitude',
                      'latitude',
                      'region',
                      'population',
                      'construction_year',
                      'extraction_type_class',
                      'management_group',
                      'quality_group',
                      'source_type',
                      'waterpoint_type']

    # import raw data
    with file_io.FileIO(x, 'r') as f:
        data = pd.read_csv(f, index_col=0)

    # subset to columns we care about
    data = data[useful_columns]

    # import transformers
    le = load_model(Config.BUCKET_NAME, Config.TRANSFORMER_PATH, 'le.joblib')
    ohc = load_model(Config.BUCKET_NAME, Config.TRANSFORMER_PATH, 'ohc.joblib')

    print("data shape: ", data.shape)
    print("Running label and one-hot encoding on the data...")
    data = le.transform(data)
    data = ohc.transform(data)
    print("New data shape: ", data.shape)

    print("uploading to GCS bucket")
    # write dataframe to csv
    data.to_csv('new_pumps_data.csv')
    # instantiate a GCS client and point to bucket
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(Config.BUCKET_NAME)

    # upload to bucket
    data_path = os.path.join(Config.IN_BATCH_DATA_PATH, 'new_pumps_data.csv')
    blob = bucket.blob(data_path)
    blob.upload_from_filename('new_pumps_data.csv')
    print('new_pumps_data.csv has been saved to: gs://{}/{}'
          .format(Config.BUCKET_NAME, Config.IN_BATCH_DATA_PATH))

    # delete local copy of data
    os.remove('new_pumps_data.csv')


pre_processing(Config.UNSEEN_DATA_FILE)


# make the request JSON body

# specify which model version to use. In the form:
# projects/project_name/models/model_name/model_version
version = 'projects/{}/models/{}/versions/{}' \
            .format(Config.PROJECT, Config.MODEL_NAME, Config.MODEL_VERSION)

# Start building the request dictionary with required information.
body = {'jobId': Config.JOB_NAME,
        'predictionInput': {
            'dataFormat': 'TEXT',
            'versionName': version,
            'maxWorkerCount': Config.MAX_WORKER_COUNT,
            'inputPaths': Config.IN_BATCH_DATA_PATH,
            'outputPath': Config.OUT_BATCH_DATA_PATH,
            'region': Config.REGION}}


# the format for this variable is projects/<your_project_name>
parent = 'projects/{}'.format(Config.PROJECT)

# submit the job
ml = discovery.build('ml', 'v1', cache_discovery=False)
request = ml.projects().jobs().create(parent=parent,
                                      body=body)
try:
    response = request.execute()
    print('Job requested.')

    # The state returned will almost always be QUEUED.
    print('state : {}'.format(response['state']))

except errors.HttpError as err:
    # Something went wrong, print out some information.
    print('There was an error getting the prediction results.' +
          'Check the details:')
    print(err._get_reason())
