# -*- coding: utf-8 -*-
"""
Created on: 12-10-2018 
@author: jason

This script runs predictions in online mode to a deployed model on CMLE using Python

"""
import os
from config import Config
from googleapiclient import discovery

# to authenticate with Google Cloud, you need to set Google Creds as an environment variable
# replace the json reference with your file
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = Config.GOOGLE_APPLICATION_CREDENTIALS
api = discovery.build('ml', 'v1')

request_data = {'instances': [
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
     1, 0, 0, 0, 0, 0, 0, 6.71, 0, 39.10, -4.68, 5.70, 2008, 1],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0,
     0, 0, 0, 1, 0, 0, 0, 0, 0, 8.22, 0, 33.24, -3.49, 6.06, 1996, 2]
]}

# the 'name' in the JSON has to be in form:
# projects/project_name/models/model_name/model_version
name = 'projects/{}/models/{}/versions/{}' \
    .format(Config.PROJECT, Config.MODEL_NAME, Config.MODEL_VERSION)

response = api.projects().predict(body=request_data, name=name).execute()
print(response)
