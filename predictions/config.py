# -*- coding: utf-8 -*-
"""
Created on: 12-10-2018 
@author: jason

# This scripts declares all variables needed to run the scripts in the scripts folder

"""
import os
from datetime import datetime


class Config(object):

    # project settings ---------------------------------------------------------------------------

    # project and overall GCP settings change these to match your environment
    PROJECT = 'pumps-cloud-ml'
    BUCKET_NAME = 'pumps-cloud-ml'
    CREDS = os.path.abspath(os.path.join(os.getcwd(), os.pardir))+'/pumps-cloud-ml-056881873ae9.json'
    GOOGLE_APPLICATION_CREDENTIALS = CREDS

    # Google CMLE job settings
    JOB_NAME = 'job_'+datetime.today().strftime('%Y%m%d_%H%M%S')
    JOB_DIR = 'gs://'+BUCKET_NAME+'/jobs/'+JOB_NAME
    PYTHON_VERSION = '2.7'
    RUNTIME_VERSION = '1.9'

    # batch prediction ---------------------------------------------------------------------------

    # settings for when running batch predictions
    MAX_WORKER_COUNT = 2
    UNSEEN_DATA_FILE = 'gs://'+BUCKET_NAME+'/data/new_pumps_data.csv'
    TRANSFORMER_PATH = 'jobs/job_20181012/transformers'
    IN_BATCH_DATA_PATH = 'jobs/data_20181012/input'
    OUT_BATCH_DATA_PATH = 'jobs/data_20181012/output'
    MODEL_VERSION = 'pumps_1_0'
    MODEL_NAME = 'pumps'







