# -*- coding: utf-8 -*-
"""
Created on: 16-OCT-2018
Author: Jason

This script contains instructions for how Google Cloud ML Engine should set up the
training job. Specify some job metadata as well as Python package dependencies from pip

"""

from setuptools import setup

packages = ['category-encoders==1.2.8',
            'urllib3',
            'h5py==2.8.0',
            'cloudml-hypertune']

setup(name='trainer',
      version='1.0',
      description='Random Forest model to predict water pump repairs in Tanzania',
      author='Jason Macwan',
      packages=['trainer'],
      install_requires=packages,
      zip_safe=False)
