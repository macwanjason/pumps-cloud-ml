# -*- coding: utf-8 -*-
"""
Created on: 16-OCT-2018
Author: Jason

This is the main entry point for the Google Cloud ML Engine application.

"""

import argparse
import hypertune
import model


if __name__ == '__main__':
  
    parser = argparse.ArgumentParser()

    # Google CloudML requires us to pass the job-dir flag to the main application
    # even if not used
    parser.add_argument(
      '--job-dir',
      help='required flag passed from setting up the Cloud ML Engine job',
      required=True
    )
    parser.add_argument(
      '--bucket',
      help='your Google Cloud Storage bucket name',
      required=True
    )
    parser.add_argument(
      '--model_dir',
      help='location to store pickled estimators',
      required=True
    )
    parser.add_argument(
      '--transformer_dir',
      help='location to store pickled transformers',
      required=True
    )

    # these are the inputs expected from the train_and_evaluate() function
  
    parser.add_argument(
        '--data',
        help='csv file to train and test the model',
        required=True
    )
    parser.add_argument(
        '--n_estimators',
        help='The number of trees in the forest.',
        type=int,
        default=100
    )
    parser.add_argument(
        '--criterion',
        help='The function to measure the quality of a split',
        default='entropy'
    )
    parser.add_argument(
        '--class_weight',
        help='Weights associated with classes',
        default='balanced_subsample'
    )

    args = parser.parse_args()
    arguments = args.__dict__
   
    estimator, accuracy, le, ohc, y_le = model.train_and_evaluate(arguments['data'],
                                                                  arguments['n_estimators'],
                                                                  arguments['criterion'],
                                                                  arguments['class_weight'])
    
    # pickle all estimators and transformers
    loc1 = model.save_model(estimator, arguments['bucket'], arguments['model_dir'], 'model.joblib')
    print("Saved estimator to: gs://{}/{}".format(args.bucket, loc1))
    loc2 = model.save_model(le, arguments['bucket'], arguments['transformer_dir'], 'le.joblib')
    print("Saved label encoder to: gs://{}/{}".format(args.bucket, loc2))
    loc3 = model.save_model(ohc, arguments['bucket'], arguments['transformer_dir'], 'ohc.joblib')
    print("Saved one-hot encoder to: gs://{}/{}".format(args.bucket, loc3))
    loc4 = model.save_model(y_le, arguments['bucket'], arguments['transformer_dir'], 'y_le.joblib')
    print("Saved y label encoder to: gs://{}/{}".format(args.bucket, loc4))

    # hypertune to optimize accuracy
    hpt = hypertune.HyperTune()
    hpt.report_hyperparameter_tuning_metric(hyperparameter_metric_tag='accuracy',
                                            metric_value=accuracy,
                                            global_step=0)
