# -*- coding: utf-8 -*-
"""
Created on: 16-OCT-2018
Author: Jason

This is the script that contains the core functionality of the training application.
It contains functions to read, clean, preprocess data and finally build a classifier

"""

from __future__ import print_function
import os
import numpy as np
import pandas as pd
import category_encoders as ce
from tensorflow.python.lib.io import file_io
from google.cloud import storage
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib   

# suppress some warnings
pd.options.mode.chained_assignment = None  # default='warn'
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

######################################################################################
# HELPER FUNCTIONS
######################################################################################


def print_nans(df):

    print('Checking for NANs:............................')
    mis_val = df.isnull().sum()
    mis_val_percent = 100 * df.isnull().sum() / len(df)
    mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
    mis_val_table_ren_columns = mis_val_table.rename(
        columns={0: 'Missing Values', 1: '% of Total Values'})
    mis_val_table_ren_columns = mis_val_table_ren_columns[
        mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
    print("Your selected dataframe has " +
          str(df.shape[1]) +
          " columns and " +
          str(len(df)) +
          " rows \n" "There are " +
          str(mis_val_table_ren_columns.shape[0]) +
          " columns that have missing values.")
    print('..............................................')
    return mis_val_table_ren_columns


def data_frame_imputer(df):
    fill = pd.Series([df[c].value_counts().index[0]
                      if df[c].dtype == np.dtype('O') else df[c].mean() for c in df],
                     index=df.columns)
    return df.fillna(fill)


def replace_with_grouped_mean(df, value, column, to_groupby):

    invalid_mask = (df[column] == value)

    # get the mean without the invalid value
    means_by_group = (df[~invalid_mask].groupby(to_groupby)[column].mean())

    # get an array of the means for all of the data
    means_array = means_by_group[df[to_groupby].values].values

    # assign the invalid values to means
    df.loc[invalid_mask, column] = means_array[invalid_mask]

    return df


def log_transformer(df, base, c=1):

    if base == 'e' or base == np.e:
        log = np.log

    elif base == '10' or base == 10:
        log = np.log10

    else:
        def log(x): return np.log(x) / np.log(base)

    c = c
    out = pd.DataFrame()
    for _ in df:
        out = df.apply(lambda x: log(x + c))
    return out


def stratified_split(x, y, test_size):

    from sklearn.model_selection import StratifiedShuffleSplit

    sss = StratifiedShuffleSplit(n_splits=10, test_size=test_size, random_state=5)
    sss.get_n_splits(x, y)
    data_train = pd.DataFrame()
    data_test = pd.DataFrame()
    label_train = pd.DataFrame()
    label_test = pd.DataFrame()
    for train_index, test_index in sss.split(x, y):
        data_train, data_test = x.iloc[train_index], x.iloc[test_index]
        label_train, label_test = y.iloc[train_index], y.iloc[test_index]
    return data_train, data_test, label_train, label_test


######################################################################################
# GET DATA
######################################################################################


def create_dataframe(x):
    """
    Imports the pumps csv data file directly from a GCS bucket.

    :param x: full GCS path to file
              e.g: 'gs://'bucket_name/data/data.csv'
    :return: two dataframes that split data and labels
    """
    # import raw data
    with file_io.FileIO(x, 'r') as f:
        raw = pd.read_csv(f, index_col=0)
      
    labels = pd.DataFrame(raw['status_group'])
    data = raw.drop('status_group', axis=1)
    
    return data, labels


######################################################################################
# CLEAN DATA
######################################################################################


def clean_data(x, y):
    """
    Takes the pumps data and label dataframe and cleans it

    :param x: the pumps dataframe
    :param y: the pumps labels dataframe
    :return:  stratified splits for train and test
    """
  
    useful_columns = ['amount_tsh',
                      'gps_height',
                      'longitude',
                      'latitude',
                      'region',
                      'population',
                      'construction_year',
                      'extraction_type_class',
                      'management_group',
                      'quality_group',
                      'source_type',
                      'waterpoint_type']

    # subset to columns we care about
    x = x[useful_columns]

    # for column construction_year, values <=1000 are probably bad
    invalid_rows = x['construction_year'] < 1000
    valid_mean = int(x.construction_year[~invalid_rows].mean())
    x.loc[invalid_rows, "construction_year"] = valid_mean

    # in some columns 0 is an invalid value
    x = replace_with_grouped_mean(df=x, value=0, column='longitude', to_groupby='region')
    x = replace_with_grouped_mean(df=x, value=0, column='population', to_groupby='region')

    # set latitude to the proper value
    x = replace_with_grouped_mean(df=x, value=-2e-8, column='latitude', to_groupby='region')

    # set amount_to non-zeroes
    x = replace_with_grouped_mean(df=x, value=0, column='amount_tsh', to_groupby='region')

    # remove na's    --projectId=${PROJECT_NUMBER} \
    x = data_frame_imputer(df=x)

    # print nans in the dataframe if any
    print_nans(x)

    # log transform numerical columns
    num_cols = ['amount_tsh', 'population']
    x[num_cols] = log_transformer(df=x[num_cols], base='e', c=1)

    # do train/test split
    x_train, x_test, y_train, y_test = stratified_split(x=x, y=y, test_size=0.2)

    return x_train, x_test, y_train, y_test


######################################################################################
# PREPROCESSING
######################################################################################


def train_pre_processing(x, y):

    """
    Preprocesses the pumps train datasets by applying label
    and one-hot encoding

    :param x: the pumps x_train dataset
    :param y: the upmps y_train dataset
    :return: encoded datasets and the fitted transformers
    """

    # transform categorical variables with encoders
    le_cols = ['region']
    ohc_cols = ['extraction_type_class',
                'management_group',
                'quality_group',
                'source_type',
                'waterpoint_type']

    # define encoders include label encoding for the actual labels
    # using handle_unknown='ignore' will leave out new unseen values so keep
    # monitoring your data for changes
    
    le = ce.OrdinalEncoder(cols=le_cols,
                           return_df=True,
                           handle_unknown='ignore')

    ohc = ce.OneHotEncoder(cols=ohc_cols,
                           return_df=True,
                           use_cat_names=False,
                           handle_unknown='ignore')

    y_le = ce.OrdinalEncoder(return_df=True,
                             handle_unknown='ignore')

    print("x_train shape: ", x.shape)
    print("y_train shape: ", y.shape)
    # apply the encoders
    print("Running label and one-hot encoding on the train data...")
    x = le.fit_transform(x)
    x = ohc.fit_transform(x)
    y = y_le.fit_transform(y)
    # update the transformers
    le = le
    ohc = ohc
    y_le = y_le
    print("Final x_train shape: ", x.shape)
    print("Final y_train shape: ", y.shape)
    print("done.")
    
    return x, y, le, ohc, y_le


def test_pre_processing(x, y, le, ohc, y_le):

    """
    Preprocesses the pumps test datasets by applying label
    and one-hot encoding

    :param x: the x_test dataset
    :param y: the y_test dataset
    :param le: the label encoder fitted from the train_pre_processing() function
    :param ohc: the one-hot encoder fitted from the train_pre_processing() function
    :param y_le: the y label encoder fitted from the train_pre_processing() function
    :return: encoded x_test and y_test
    """
  
    print("x_test shape: ", x.shape)
    print("y_test shape: ", y.shape)
    print("Running label and one-hot encoding on the test data...")
    x = le.transform(x)
    x = ohc.transform(x)
    y = y_le.transform(y)
    print("New x_test shape: ", x.shape)
    print("New y_test shape: ", y.shape)
    print("done.")

    return x, y


######################################################################################
# TRAIN MODEL
######################################################################################

def train_and_evaluate(x, n_estimators=100, criterion='entropy', class_weight='balanced_subsample'):
    """
    A full pipeline that cleans, preprocesses data and then fits a
    random forest classifier

    :param x              : full GCS path to file
                            e.g: 'gs://'bucket_name/data/data.csv'
    :param n_estimators:  : random forest parameter for number of branches
    :param criterion:     : random forest parameter for node splitting methodology
    :param class_weight:  : random forest parameter whether to treat all classes as balanced
    :return               :  all estimator/transfromer objects and the accuracy metric
    """

    # ingest and process data
    data, labels = create_dataframe(x=x)
    x_train, x_test, y_train, y_test = clean_data(x=data, y=labels)
    x_train, y_train, le, ohc, y_le = train_pre_processing(x=x_train, y=y_train)
    x_test, y_test = test_pre_processing(x=x_test, y=y_test, le=le, ohc=ohc, y_le=y_le)
    
    # train classifier
    print("training classifier...")
    rf = RandomForestClassifier(n_estimators=n_estimators,
                                criterion=criterion,
                                class_weight=class_weight)
    rf.fit(x_train, np.ravel(y_train))
    print(" classifier has been trained")
    
    # evaluate on test set
    test_pred = rf.predict(x_test)
    accuracy = accuracy_score(y_test, test_pred)
    print("Test Accuracy: ", accuracy)
    
    # we need to return the transformers also so that it gets captured in the global scope
    # this will allow us to pass these objects to the save_model() function
    return rf, accuracy, le, ohc, y_le


######################################################################################
# EXPORT PICKLED OBJECTS
######################################################################################


def save_model(estimator, bucket, gcspath, file_name):

    """
    :param estimator : estimator or transformer currently in memory to pickle
    :param bucket    : your Google Cloud Storage bucket name
    :param gcspath   : path in the bucket where to store file
                       without gs:// or the bucket name
    :param file_name : name of the pickled file
    :return          : file path on GCS where the pickled object is stored
    """

    # create the pickle file
    joblib.dump(estimator, file_name)

    # instantiate a GCS client and point to bucket
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket)

    # upload to bucket
    model_path = os.path.join(gcspath, file_name)
    blob = bucket.blob(model_path)
    blob.upload_from_filename(file_name)

    # delete local file
    os.remove(file_name)

    return model_path
